/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.shpaeproject;

/**
 *
 * @author Acer
 */
public class Triangle {
    private double b;
    private double h_b;
    
    public Triangle(double b, double h_b){
        this.b = b;
        this.h_b = h_b;
    }
    
    public double calArea(){
        return (b*h_b)/2.0;
    }
    
    public double b(){
        return b;
    }
    
    public double h_b(){
        return h_b;
    }
    
    public void setB(double b){
        if(b <= 0){
            System.out.println("Error: Length of base must more than 0");
            return;
        }
        this.b = b;
    }
    
    public void setH_B(double h_b){
        if(h_b <= 0){
            System.out.println("Error: Height of base must more than 0");
            return;
        }
        this.h_b = h_b;
    }
    
    public String toString(){
        return "Area of triangle(b = " + this.b +" h_b = " + this.h_b + " is " + this.calArea();
    }
}
