/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.shpaeproject;

/**
 *
 * @author Acer
 */
public class Rectangle {
    private double l;
    private double w;
    
    public Rectangle(double l, double w){
        this.l = l;
        this.w = w;
    }
    
    public double calArea(){
        return l*w;
    }
    
    public double getL(){
        return l;
    }
    
    public double getW(){
        return w;
    }
    
    public void setL(double l){
        if(l <= 0){
            System.out.println("Error: Length must more than 0");
            return;
        }
        if(l <= w){
            System.out.println("Error: Length must more than width");
            return;
        }
        this.l = l;
    }
    
    public void setW(double w){
        if(w <= 0){
            System.out.println("Error: Width must more than 0");
            return;
        }
        if(w >= l){
            System.out.println("Error: Width must less than length");
            return;
        }
        this.w = w;
    }
    
    public String toString(){
        return "Area of retangle(l = " + this.getL() + " w = " + this.getW() + ") is " + this.calArea();
    }
}
