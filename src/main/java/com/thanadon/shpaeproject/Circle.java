/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.shpaeproject;

/**
 *
 * @author Acer
 */
public class Circle {
    private double r;
    public static final double pi = 22.0/7;
    
    public Circle(double r){     
        this.r = r;
    }
    
    public double calArea(){
        return pi*r*r;
    }
    
    public void setR(double r){
        if(r <= 0){
            System.out.println("Error: Radius must more than 0");
            return;       
        }
        this.r = r;
    }
    
    public double getR(){
        return r;
    }
    
    public String toString(){
        return "Area of circle(r = " + this.getR() + ") is " + this.calArea();
    }
        
    
}
